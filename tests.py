from main import ingest_json


def test_ingest_json():
    filename = "test.json"
    json_file_dict = ingest_json(filename)
    assert type(json_file_dict) == dict
    assert "vulnerabilities" in json_file_dict
    assert type(json_file_dict["vulnerabilities"]) == list
